# 2.0.0

- BREAKING: Missing migration files/classes will make `yramid` now fail by default. To restore previous behavior you'll need to add the setting `'relax' => true` under the migrations config
- BREAKING: The key `pdo` has been removed from the config. Use `connection` instead
- BREAKING: Remove the `Config` interface. Only arrays are allowed as config structure

# 1.1.1

- Make the `Yramid\ConfigAccessor` part of the API
- Add a documented config example (`config.example.php`)


# 1.1.0

- Allow usage of a config array instead of a `Yramid\Config` instance
- `Yramid\Config` is deprecated and will be removed in 2.0.0
- FIX: Allow underscores in names for seeds and migrations


# 1.0.0

Fist stable release

