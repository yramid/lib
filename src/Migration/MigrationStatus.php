<?php

declare(strict_types=1);

namespace Yramid\Migration;

// @codingStandardsIgnoreStart

/**
 * @api
 */
final class MigrationStatus
{
    public const NEW = 'NEW';
    public const UP = 'UP';
    public const DOWN = 'DOWN';
    public const MISSING = 'MISSING';
}
