<?php

declare(strict_types=1);

namespace Yramid\Migration;

use Yramid\Migration;

/**
 * @api
 * @readonly
 */
class MigrationData
{
    /**
     * @param positive-int $serial
     * @param non-empty-string|string $name
     * @param string $status
     * @param non-empty-string|null $timestamp
     * @param non-empty-string|null $savepoint
     * @param non-empty-string|null $fileName
     * @param class-string<Migration>|null $className
     */
    public function __construct(
        public int $serial,
        public string $name,
        public string $status,
        public ?string $timestamp,
        public ?string $savepoint,
        public ?string $fileName,
        public ?string $className,
    ) {
    }
}
