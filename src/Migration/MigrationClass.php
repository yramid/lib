<?php

declare(strict_types=1);

namespace Yramid\Migration;

use Yramid\Migration;

/**
 * @internal
 * @readonly
 */
final class MigrationClass
{
    /**
     * @param positive-int $serial
     * @param non-empty-string $name
     * @param non-empty-string $fileName
     * @param class-string<Migration> $className
     */
    public function __construct(
        public int $serial,
        public string $name,
        public string $fileName,
        public string $className,
    ) {
    }
}
