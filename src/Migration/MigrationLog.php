<?php

declare(strict_types=1);

namespace Yramid\Migration;

/**
 * @internal
 * @readonly
 */
final class MigrationLog
{
    /**
     * @param positive-int $serial
     * @param non-empty-string $name
     * @param non-empty-string|null $timestamp
     * @param non-empty-string|null $savepoint
     */
    public function __construct(
        public int $serial,
        public string $name,
        public ?string $timestamp,
        public ?string $savepoint,
    ) {
    }
}
