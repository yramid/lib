<?php

declare(strict_types=1);

namespace Yramid\Migration;

use DateTimeInterface;
use PDO;
use Yramid\ConfigAccessor;
use Yramid\Exception\UnknownSavepoint;
use Yramid\Exception\UnknownSerial;

/**
 * @internal
 */
final class MigrationLogs
{
    public static function setUp(array $config): void
    {
        ConfigAccessor::getPdo($config)->query(<<<SQL
            CREATE TABLE IF NOT EXISTS yramid_log (
                serial BIGINT PRIMARY KEY,
                name TEXT NOT NULL,
                timestamp TIMESTAMP,
                savepoint TEXT
            )
        SQL);
    }

    public static function getStatus(array $config, int $serial): string
    {
        $stmt = ConfigAccessor::getPdo($config)
            ->prepare('SELECT timestamp FROM yramid_log WHERE serial = :serial');

        $stmt->execute(['serial' => $serial]);

        return match ($stmt->fetchColumn()) {
            false => MigrationStatus::NEW,
            null => MigrationStatus::DOWN,
            default => MigrationStatus::UP,
        };
    }

    public static function up(
        array $config,
        MigrationData $migration,
    ): void {
        $params = [
            'serial' => $migration->serial,
            'name' => $migration->name,
            'timestamp' => date(DateTimeInterface::ISO8601),
        ];

        $pdo = ConfigAccessor::getPdo($config);

        $update = 'UPDATE yramid_log SET name = :name, timestamp = :timestamp WHERE serial = :serial';
        $insert = 'INSERT INTO yramid_log (serial, name, timestamp) VALUES (:serial, :name, :timestamp)';

        $currentStatus = self::getStatus($config, $migration->serial);
        $statement = $currentStatus === MigrationStatus::NEW
            ? $pdo->prepare($insert)
            : $pdo->prepare($update);

        $statement->execute($params);
    }

    public static function down(
        array $config,
        int $serial,
    ): void {
        ConfigAccessor::getPdo($config)
            ->prepare('UPDATE yramid_log SET timestamp = null WHERE serial = ?')
            ->execute([$serial]);
    }

    /**
     * @param array $config
     * @param positive-int|non-empty-string $target
     * @param non-empty-string|null $savepoint
     */
    public static function setSavepoint(
        array $config,
        int|string $target,
        ?string $savepoint,
    ): void {
        $serial = is_string($target)
            ? self::getSerialFor($config, $target)
            : $target;

        if (!isset(self::getAll($config)[$serial])) {
            throw new UnknownSerial($serial);
        }

        if ($savepoint !== null) {
            self::unsetSavepoint($config, $savepoint);
        }

        ConfigAccessor::getPdo($config)
            ->prepare('UPDATE yramid_log SET savepoint = ? WHERE serial = ?')
            ->execute([$savepoint, $serial]);
    }

    /**
     * @param array $config
     * @param non-empty-string $savepoint
     */
    private static function unsetSavepoint(
        array $config,
        string $savepoint,
    ): void {
        ConfigAccessor::getPdo($config)
            ->prepare('UPDATE yramid_log SET savepoint = null WHERE savepoint = ?')
            ->execute([$savepoint]);
    }

    /**
     * @param array $config
     * @param non-empty-string $savepoint
     *
     * @return positive-int
     */
    public static function getSerialFor(array $config, string $savepoint): int
    {
        $stmt = ConfigAccessor::getPdo($config)->prepare(
            'SELECT serial FROM yramid_log WHERE savepoint = :savepoint',
        );

        $stmt->execute(['savepoint' => $savepoint]);

        /** @var positive-int|false $serial */
        $serial = $stmt->fetchColumn();

        if ($serial === false) {
            throw new UnknownSavepoint($savepoint);
        }

        return (int) $serial;
    }

    /**
     * @param array $config
     * @param non-empty-string $savepoint
     *
     * @return positive-int|null
     */
    public static function getNextSerialFor(array $config, string $savepoint): ?int
    {
        $stmt = ConfigAccessor::getPdo($config)->prepare(
            'SELECT serial FROM yramid_log WHERE serial > :serial ORDER BY SERIAL LIMIT 1',
        );

        $stmt->execute(['serial' => self::getSerialFor($config, $savepoint)]);

        /** @var positive-int|string|false $result */
        $result = $stmt->fetchColumn();

        /** @var positive-int|null $serial */
        $serial = $result === false
            ? null
            : (int) $result;

        return $serial;
    }

    /**
     * @param array $config
     * @return array<positive-int, MigrationLog>
     */
    public static function getAll(array $config): array
    {
        $logs = [];

        $statement = ConfigAccessor::getPdo($config)->prepare(
            'SELECT serial, name, timestamp, savepoint FROM yramid_log'
        );
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $statement->execute();

        /** @var array{
         *     serial: non-empty-string,
         *     name: non-empty-string,
         *     timestamp: non-empty-string|null,
         *     savepoint: non-empty-string|null
         *  } $item
         */
        foreach ($statement as $item) {
            /** @var positive-int $serial */
            $serial = (int) $item['serial'];

            $logs[$serial] = new MigrationLog(
                $serial,
                $item['name'],
                $item['timestamp'],
                $item['savepoint'],
            );
        }

        return $logs;
    }
}
