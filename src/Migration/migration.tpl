<?php

declare(strict_types=1);

namespace #NAMESPACE#;

use PDO;
use Yramid\Migration;

class #NAME# implements Migration
{
    public static function up(PDO $pdo): void
    {
        // @todo Implement #NAME#::up()
    }

    public static function down(PDO $pdo): void
    {
        // @todo Implement #NAME#::down()
    }
}
