<?php

declare(strict_types=1);

namespace Yramid;

use PDO;
use Yramid\Exception\InvalidConfigValue;

use function call_user_func;

/**
 * @api
 */
final class ConfigAccessor
{
    private const NAMESPACE = '{^\w+(?:\\\\\w+)*$}';

    private static ?PDO $pdo = null;

    public static function getPdo(array $config): PDO
    {
        if (self::$pdo) {
            return self::$pdo;
        }

        /** @var PDO|non-empty-string|(callable(): PDO)|null $connectionConfig */
        $connectionConfig = $config['connection'] ?? null;

        if ($connectionConfig instanceof PDO) {
            return self::$pdo = $connectionConfig;
        } elseif (is_callable($connectionConfig)) {
            return self::$pdo = call_user_func($connectionConfig);
        } elseif (is_string($connectionConfig)) {
            return self::$pdo = new PDO($connectionConfig);
        }

        throw new InvalidConfigValue('Missing or invalid connection');
    }

    public static function relaxAboutMissingMigrations(array $config): bool
    {
        return (bool) ($config['migrations']['relax'] ?? false);
    }

    /**
     * @param array $config
     *
     * @return non-empty-string
     */
    public static function getMigrationPath(array $config): string
    {
        return self::validPath('migrations', $config);
    }

    /**
     * @param array $config
     *
     * @return non-empty-string
     */
    public static function getMigrationNamespace(array $config): string
    {
        return self::validNamespace('migrations', $config);
    }

    /**
     * @param array $config
     *
     * @return non-empty-string
     */
    public static function getMigrationTemplate(array $config): string
    {
        return self::validTemplateFile(
            'migrations',
            $config,
            __DIR__ . '/Migration/migration.tpl',
        );
    }

    /**
     * @param array $config
     *
     * @return non-empty-string
     */
    public static function getSeedPath(array $config): string
    {
        return self::validPath('seeds', $config);
    }

    /**
     * @param array $config
     *
     * @return non-empty-string
     */
    public static function getSeedNamespace(array $config): string
    {
        return self::validNamespace('seeds', $config);
    }

    /**
     * @param array $config
     *
     * @return non-empty-string
     */
    public static function getSeedTemplate(array $config): string
    {
        return self::validTemplateFile(
            'seeds',
            $config,
            __DIR__ . '/Seed/seed.tpl',
        );
    }

    /**
     * @param non-empty-string $scope
     * @param array $config
     *
     * @return non-empty-string
     */
    private static function validNamespace(string $scope, array $config): string
    {
        /** @var non-empty-string|null $namespace */
        $namespace = $config[$scope]['namespace'] ?? null;

        if ($namespace === null) {
            throw new InvalidConfigValue("Missing $scope namespace");
        }

        if (!preg_match(self::NAMESPACE, $namespace)) {
            throw new InvalidConfigValue("Invalid $scope namespace: $namespace");
        }

        return $namespace;
    }

    /**
     * @param non-empty-string $scope
     * @param array $config
     *
     * @return non-empty-string
     */
    private static function validPath(string $scope, array $config): string
    {
        /** @var string|null $path */
        $path = $config[$scope]['path'] ?? null;

        if ($path === null || $path === '') {
            throw new InvalidConfigValue("Invalid or missing $scope path: $path");
        }

        return $path;
    }

    /**
     * @param non-empty-string $scope
     * @param array $config
     * @param non-empty-string $default
     *
     * @return non-empty-string
     */
    private static function validTemplateFile(string $scope, array $config, string $default): string
    {
        /** @var non-empty-string $templateFile */
        $templateFile = $config[$scope]['template'] ?? $default;

        if (!is_file($templateFile)) {
            throw new InvalidConfigValue("Invalid $scope template file: $templateFile");
        }

        return $templateFile;
    }
}
