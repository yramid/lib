<?php

declare(strict_types=1);

namespace Yramid;

use Closure;
use Yramid\Migration\MigrationData;
use Yramid\Seed\SeedData;

/**
 * @api
 */
interface YramidInterface
{
    public function setUp(array $config): void;

    /**
     * @param array $config
     * @param non-empty-string $name
     *
     * @return SeedData
     */
    public function createSeed(array $config, string $name): SeedData;

    /**
     * @param array $config
     * @param non-empty-string $name
     * @param bool $dryRun
     */
    public function runSeed(array $config, string $name, bool $dryRun): void;

    /**
     * @param array $config
     * @param positive-int $serial
     * @param non-empty-string $name
     *
     * @return MigrationData
     */
    public function createMigration(array $config, int $serial, string $name): MigrationData;

    /**
     * @param array $config
     * @param positive-int|non-empty-string $target
     * @param bool $dryRun
     * @param Closure|null $postMigrationCallback
     */
    public function migrateUp(
        array $config,
        int|string $target,
        bool $dryRun,
        ?Closure $postMigrationCallback = null,
    ): void;

    /**
     * @param array $config
     * @param positive-int|non-empty-string $target
     * @param bool $dryRun
     * @param Closure|null $postMigrationCallback
     */
    public function migrateDown(
        array $config,
        int|string $target,
        bool $dryRun,
        ?Closure $postMigrationCallback = null,
    ): void;

    /**
     * @param array $config
     * @param positive-int|non-empty-string $target
     * @param non-empty-string|null $savepoint
     */
    public function setMigrationSavepoint(
        array $config,
        int|string $target,
        ?string $savepoint,
    ): void;

    /**
     * @param array $config
     * @return array<positive-int, MigrationData>
     */
    public function getMigrationStatus(array $config): array;
}
