<?php

declare(strict_types=1);

namespace Yramid\Seed;

use Yramid\Seed;

/**
 * @api
 * @readonly
 */
final class SeedData
{
    /**
     * @param non-empty-string $name
     * @param non-empty-string $fileName
     * @param class-string<Seed> $className
     */
    public function __construct(
        public string $name,
        public string $fileName,
        public string $className,
    ) {
    }
}
