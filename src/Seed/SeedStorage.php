<?php

declare(strict_types=1);

namespace Yramid\Seed;

use FilesystemIterator;
use Yramid\ConfigAccessor;
use Yramid\Exception\RuntimeException;
use SplFileInfo;
use Yramid\Seed;
use Yramid\Util;

/**
 * @internal
 */
final class SeedStorage
{
    public static function setUp(array $config): void
    {
        $seedPath = ConfigAccessor::getSeedPath($config);

        if (is_dir($seedPath)) {
            return;
        }

        mkdir(
            $seedPath,
            recursive: true,
        );
    }

    /**
     * @param string $path
     * @param string $namespace
     * @param array<non-empty-string, SeedData> $agg
     *
     * @return array<non-empty-string, SeedData>
     */
    private static function doList(string $path, string $namespace, array $agg): array
    {
        $fileIterator = new FilesystemIterator(
            $path,
            FilesystemIterator::SKIP_DOTS,
        );

        /** @var SplFileInfo $fileInfo */
        foreach ($fileIterator as $fileInfo) {
            /** @var non-empty-string $pathName */
            $pathName = $fileInfo->getPathname();

            if ($fileInfo->isFile()) {
                $isFileNameValid = preg_match(
                    '/^(?<name>[a-zA-Z0-9]+)\.php$/i',
                    basename($pathName),
                    $matches,
                );

                if (!$isFileNameValid) {
                    continue;
                }

                /** @var non-empty-string $name */
                $name = $matches['name'];

                /** @var class-string<Seed> $className */
                $className = "$namespace\\$name";

                $agg[$name] = new SeedData(
                    $name,
                    $pathName,
                    $className,
                );
            } elseif ($fileInfo->isDir()) {
                $agg = self::doList($pathName, $namespace, $agg);
            }
        }

        return $agg;
    }

    /**
     * @param array $config
     *
     * @return array<non-empty-string, SeedData>
     */
    public static function list(array $config): array
    {
        $list = self::doList(
            ConfigAccessor::getSeedPath($config),
            ConfigAccessor::getSeedNamespace($config),
            [],
        );

        ksort($list);

        return $list;
    }

    /**
     * @param array $config
     * @param non-empty-string $name
     *
     * @return SeedData
     */
    public static function create(array $config, string $name): SeedData
    {
        $existingSeeds = self::list($config);

        $name = Util::filterName($name);

        if (isset($existingSeeds[$name])) {
            throw new RuntimeException('There is already a seed named ' . $name);
        }

        /**
         * @psalm-suppress UnnecessaryVarAnnotation
         * @var class-string<Seed> $className
         */
        [$className, $body] = Util::createClass(
            $name,
            ConfigAccessor::getSeedNamespace($config),
            ConfigAccessor::getSeedTemplate($config),
        );

        $seedPath = ConfigAccessor::getSeedPath($config);
        $fileName = "{$seedPath}/{$name}.php";

        file_put_contents(
            $fileName,
            $body,
        );

        return new SeedData(
            $name,
            $fileName,
            $className,
        );
    }
}
