<?php

declare(strict_types=1);

namespace Yramid;

use PDO;

/**
 * @api
 */
interface Seed
{
    public static function run(PDO $pdo): void;
}
