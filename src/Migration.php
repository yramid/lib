<?php

declare(strict_types=1);

namespace Yramid;

use PDO;

/**
 * @api
 */
interface Migration
{
    public static function up(PDO $pdo): void;
    public static function down(PDO $pdo): void;
}
