<?php

declare(strict_types=1);

namespace Yramid\Exception;

class UnknownSavepoint extends RuntimeException
{
    public function __construct(string $savepoint)
    {
        parent::__construct(
            "Unknown savepoint $savepoint",
        );
    }
}
