<?php

declare(strict_types=1);

namespace Yramid\Exception;

class UnknownSerial extends RuntimeException
{
    public function __construct(int $serial)
    {
        parent::__construct(
            "Unknown serial $serial",
        );
    }
}
