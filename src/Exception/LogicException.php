<?php

declare(strict_types=1);

namespace Yramid\Exception;

class LogicException extends \LogicException
{
}
