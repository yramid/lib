<?php

declare(strict_types=1);

namespace Yramid\Exception;

use Yramid\Migration\MigrationData;

class MissingMigration extends LogicException
{
    public function __construct(public MigrationData $migration)
    {
        parent::__construct(
            "Missing migration file/class for $migration->serial $migration->name",
        );
    }
}
