<?php

declare(strict_types=1);

namespace Yramid\Test\Migration;

use Yramid\ConfigAccessor;
use Yramid\Exception\LogicException;
use Yramid\Exception\UnknownSerial;
use Yramid\Migration\MigrationStatus;
use Yramid\Test\Fixtures\Scenario;
use Yramid\Test\TestCase;

/**
 * @testdox Yramid
 */
class DownTest extends TestCase
{
    protected function getConfig(): array
    {
        return Scenario::create(
            [
                [100, 'Migration100', date('c'), null],
                [300, 'Migration300', date('c'), null],
                [500, 'Migration500', date('c'), null],
                [600, 'Migration600', date('c'), null], // <- missing
                [700, 'Migration700', null, null],
                // [900, 'Migration900', null, null], // <- new
            ],
            [],
            [
                '123_InvalidFile.sql' => '',
                '100_Migration100.php' => Scenario::migrationClass(
                    'Migration100',
                    'public static function down(\PDO $pdo): void { throw new \RuntimeException(__METHOD__); }',
                ),
                '300_Migration300.php' => Scenario::migrationClass('Migration300'),
                '500_Migration500.php' => Scenario::migrationClass('Migration500'),
                'subidr' => ['700_Migration700.php' => Scenario::migrationClass('Migration700')],
                '900_Migration900.php' => Scenario::migrationClass('Migration900'),
            ],
        );
    }

    /**
     * @test
     * @testdox migrateDown() fails on invalid serial
     */
    public function migrateDownInvalidSerial(): void
    {
        $this->expectExceptionObject(new UnknownSerial(999));
        $this->subject->migrateDown(
            $this->config,
            999,
            false,
        );
    }

    /**
     * @test
     * @testdox migrateDown() fails on invalid serial
     */
    public function migrateFailsOnInvalidFile(): void
    {
        file_put_contents(
            ConfigAccessor::getMigrationPath($this->config) . '/800_Fail.php',
            '<?php namespace ' . ConfigAccessor::getMigrationNamespace($this->config) . '; class Fail {}',
        );

        ConfigAccessor::getPdo($this->config)->query(
            'INSERT INTO yramid_log (serial, name, timestamp) VALUES (800, \'Fail\', \'2020-02-02\')',
        );

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Class \'Yramid\Test\Migration\Fail\' is invalid or does not exist');
        $this->subject->migrateDown(
            $this->config,
            800,
            false,
        );
    }

    /**
     * @test
     * @testdox migrateDown() works as expected
     */
    public function migrateDown(): void
    {
        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            900 => MigrationStatus::NEW,
        ]);

        $this->subject->migrateDown(
            $this->config,
            300,
            false,
            $this->postMigrateCallback($callbackLog),
        );

        $this->assertSame(
            [
                900 => [MigrationStatus::NEW, MigrationStatus::NEW],
                700 => [MigrationStatus::DOWN, MigrationStatus::DOWN],
                600 => [MigrationStatus::MISSING, MigrationStatus::MISSING],
                500 => [MigrationStatus::UP, MigrationStatus::DOWN],
                300 => [MigrationStatus::UP, MigrationStatus::DOWN],
            ],
            $callbackLog,
        );

        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::DOWN,
            500 => MigrationStatus::DOWN,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            900 => MigrationStatus::NEW,
        ]);

        $this->assertTestLog([
            'Migration500::down',
            'Migration300::down',
        ]);
    }

    /**
     * @test
     * @testdox migrateDown() works as expected - dry-run
     */
    public function migrateDownDryRun(): void
    {
        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            900 => MigrationStatus::NEW,
        ]);

        $this->subject->migrateDown(
            $this->config,
            300,
            true,
            $this->postMigrateCallback($callbackLog),
        );

        $this->assertSame(
            [
                900 => [MigrationStatus::NEW, MigrationStatus::NEW],
                700 => [MigrationStatus::DOWN, MigrationStatus::DOWN],
                600 => [MigrationStatus::MISSING, MigrationStatus::MISSING],
                500 => [MigrationStatus::UP, MigrationStatus::DOWN],
                300 => [MigrationStatus::UP, MigrationStatus::DOWN],
            ],
            $callbackLog,
        );

        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            900 => MigrationStatus::NEW,
        ]);

        $this->assertTestLog([]);
    }

    /**
     * @test
     * @testdox migrateDown() rolls back everything on exception
     */
    public function migrateDownException(): void
    {
        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            900 => MigrationStatus::NEW,
        ]);

        $callbackLog = [];

        try {
            $this->subject->migrateDown(
                $this->config,
                100,
                false,
                $this->postMigrateCallback($callbackLog),
            );
            $this->fail('No exception thrown');
        } catch (\RuntimeException) {
            // everything ok
        }

        $this->assertSame(
            [
                900 => [MigrationStatus::NEW, MigrationStatus::NEW],
                700 => [MigrationStatus::DOWN, MigrationStatus::DOWN],
                600 => [MigrationStatus::MISSING, MigrationStatus::MISSING],
                500 => [MigrationStatus::UP, MigrationStatus::DOWN],
                300 => [MigrationStatus::UP, MigrationStatus::DOWN],
            ],
            $callbackLog,
            'lelele',
        );

        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            900 => MigrationStatus::NEW,
        ]);

        $this->assertTestLog([]);
    }
}
