<?php

declare(strict_types=1);

namespace Yramid\Test\Migration;

use Yramid\Exception\UnknownSavepoint;
use Yramid\Exception\UnknownSerial;
use Yramid\Migration\MigrationStatus;
use Yramid\Test\Fixtures\Scenario;
use Yramid\Test\TestCase;

/**
 * @testdox Yramid
 */
class SavepointTest extends TestCase
{
    protected function getConfig(): array
    {
        return Scenario::create(
            [
                [100, 'SavepointMigration100', date('c'), 'lower'],
                [200, 'SavepointMigration200', date('c'), null],
                [300, 'SavepointMigration300', null, 'upper'],
                [400, 'SavepointMigration400', null, null],
            ],
            [],
            [
                ['100_SavepointMigration100.php' => Scenario::migrationClass('SavepointMigration100')],
                ['200_SavepointMigration200.php' => Scenario::migrationClass('SavepointMigration200')],
                ['300_SavepointMigration300.php' => Scenario::migrationClass('SavepointMigration300')],
                ['400_SavepointMigration400.php' => Scenario::migrationClass('SavepointMigration400')],
            ],
        );
    }

    /**
     * @test
     * @testdox setMigrationSavepoint() fails on invalid serial
     */
    public function setMigrationSavepointInvalidSerial(): void
    {
        $this->expectExceptionObject(new UnknownSerial(123));
        $this->subject->setMigrationSavepoint(
            $this->config,
            123,
            __FUNCTION__,
        );
    }

    /**
     * @test
     * @testdox migrateUp() fails on invalid savepoint
     */
    public function migrateUpOnInvalidSavepoint(): void
    {
        $this->expectExceptionObject(new UnknownSavepoint(__FUNCTION__));
        $this->subject->migrateUp(
            $this->config,
            __FUNCTION__,
            false,
        );
    }

    /**
     * @test
     * @testdox migrateUp() to savepoint works as expected
     */
    public function migrateUp(): void
    {
        $this->assertStatus([
            100 => MigrationStatus::UP,
            200 => MigrationStatus::UP,
            300 => MigrationStatus::DOWN,
            400 => MigrationStatus::DOWN,
        ]);

        $this->subject->migrateUp($this->config, 'upper', false);

        $this->assertStatus([
            100 => MigrationStatus::UP,
            200 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            400 => MigrationStatus::DOWN,
        ]);
    }

    /**
     * @test
     * @testdox migrateDown() fails on invalid savepoint
     */
    public function migrateUpDownInvalidSavepoint(): void
    {
        $this->expectExceptionObject(new UnknownSavepoint(__FUNCTION__));
        $this->subject->migrateDown(
            $this->config,
            __FUNCTION__,
            false,
        );
    }

    /**
     * @test
     * @testdox migrateDown() to savepoint works as expected
     */
    public function migrateDown(): void
    {
        $this->assertStatus([
            100 => MigrationStatus::UP,
            200 => MigrationStatus::UP,
            300 => MigrationStatus::DOWN,
            400 => MigrationStatus::DOWN,
        ]);

        $this->subject->migrateDown($this->config, 'lower', false);

        $this->assertStatus([
            100 => MigrationStatus::UP,
            200 => MigrationStatus::DOWN,
            300 => MigrationStatus::DOWN,
            400 => MigrationStatus::DOWN,
        ]);
    }

    /**
     * @test
     * @test (un)setMigrationSavepoint() works as intended
     */
    public function setMigrationSavepoint(): void
    {
        $this->assertSavepoints([
            'lower' => 100,
            'upper' => 300,
        ]);

        // set new savepoint
        $this->subject->setMigrationSavepoint(
            $this->config,
            200,
            'middle',
        );

        $this->assertSavepoints([
            'lower' => 100,
            'middle' => 200,
            'upper' => 300,
        ]);

        // move existing savepoint
        $this->subject->setMigrationSavepoint(
            $this->config,
            400,
            'middle',
        );

        $this->assertSavepoints([
            'lower' => 100,
            'upper' => 300,
            'middle' => 400,
        ]);

        // overwrite existing savepoint
        $this->subject->setMigrationSavepoint(
            $this->config,
            400,
            'upper',
        );

        $this->assertSavepoints([
            'lower' => 100,
            'upper' => 400,
        ]);

        $this->subject->setMigrationSavepoint(
            $this->config,
            'upper',
            null,
        );

        $this->assertSavepoints([
            'lower' => 100,
        ]);
    }

    private function assertSavepoints(array $expected): void
    {
        $savepoints = array_column(
            $this->subject->getMigrationStatus($this->config),
            'serial',
            'savepoint',
        );

        unset($savepoints['']);

        $this->assertSame(
            $expected,
            $savepoints,
        );
    }
}
