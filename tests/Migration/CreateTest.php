<?php

declare(strict_types=1);

namespace Yramid\Test\Migration;

use Yramid\Exception\InvalidName;
use Yramid\Exception\LogicException;
use Yramid\Migration;
use Yramid\Test\Fixtures\Scenario;
use Yramid\Test\TestCase;

/**
 * @testdox Yramid
 */
class CreateTest extends TestCase
{
    protected function getConfig(): array
    {
        return Scenario::create(
            [],
            [],
            [
                '100_Migration100.php' => Scenario::migrationClass('Migration100'),
            ],
        );
    }

    /**
     * @test
     * @testdox createMigration() creates a valid migration stub class
     */
    public function createMigration(): void
    {
        $serial = random_int(10_000, 20_000);

        $migrationData = $this->subject->createMigration(
            $this->config,
            $serial,
            "Migration$serial",
        );

        $this->assertFileExists($migrationData->fileName);
        require $migrationData->fileName;

        $this->assertTrue(class_exists($migrationData->className));
        $this->assertTrue(
            is_a(
                $migrationData->className,
                Migration::class,
                true,
            ),
        );
    }

    /**
     * @test
     * @testdox createMigration() fails on colliding serial
     */
    public function createMigrationSerialCollision(): void
    {
        $serial = random_int(10_000, 20_000);

        $migrationData = $this->subject->createMigration(
            $this->config,
            $serial,
            "MigrationOne$serial",
        );

        $this->expectExceptionObject(
            new LogicException('There is already a migration with serial ' . $migrationData->serial),
        );

        $this->subject->createMigration(
            $this->config,
            $migrationData->serial,
            "MigrationTwo$serial",
        );
    }

    /**
     * @test
     * @testdox createMigration() fails on colliding name
     */
    public function createMigrationNameCollision(): void
    {

        $migrationData = $this->subject->createMigration(
            $this->config,
            1,
            "MigrationOne",
        );

        $this->expectExceptionObject(
            new LogicException('There is already a migration with name ' . $migrationData->name),
        );

        $this->subject->createMigration(
            $this->config,
            2,
            $migrationData->name,
        );
    }

    /**
     * @test
     * @testdox createMigration() fails on invalid name
     */
    public function createMigrationInvalidName(): void
    {
        $this->expectExceptionObject(new InvalidName('invalid_migration'));
        $this->subject->createMigration(
            $this->config,
            1224,
            'invalid_migration',
        );
    }
}
