<?php

declare(strict_types=1);

namespace Yramid\Test;

use PDO;

return [
    'connection' => new PDO('sqlite::memory:'),
    'migrations' => [
        'path' => 'vfs://root/migrations',
        'namespace' => 'Yramid\Test\Migration',
        'relax' => true,
    ],
    'seeds' => [
        'path' => 'vfs://root/seeds',
        'namespace' => 'Yramid\Test\Seeds',
    ],
];
