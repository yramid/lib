<?php

declare(strict_types=1);

namespace Yramid\Test\Seed;

use Yramid\Exception\InvalidName;
use Yramid\Exception\RuntimeException;
use Yramid\Test\Fixtures\Scenario;
use Yramid\Test\TestCase;

/**
 * @testdox Yramid
 */
class SeedTest extends TestCase
{
    protected function getConfig(): array
    {
        return Scenario::create(
            [],
            [
                'FileToBeIgnored' => '',
                'SeedOne.php' => Scenario::seedClass('SeedOne'),
                'subdir' => [
                    'SeedTwo.php' => Scenario::seedClass('SeedTwo'),
                ],
                'FailingSeed.php' => Scenario::seedClass(
                    'FailingSeed',
                    'public static function run(\PDO $pdo): void { throw new \RuntimeException(__METHOD__); }',
                ),
            ],
            [],
        );
    }

    /**
     * @test
     * @testdox runSeed() can be called with seed file in seed path
     */
    public function runSeed(): void
    {
        $this->subject->runSeed(
            $this->config,
            'SeedOne',
            false,
        );

        $this->assertTestLog(['SeedOne::run']);
    }

    /**
     * @test
     * @testdox runSeed() can be called with seed file in a subdirectory of the seed path
     */
    public function runSeedInSubDir(): void
    {
        $this->subject->runSeed(
            $this->config,
            'SeedTwo',
            false,
        );

        $this->assertTestLog(['SeedTwo::run']);
    }

    /**
     * @test
     * @testdox runSeed() rolls back all changes in dry-run
     */
    public function runSeedDryMode(): void
    {
        $this->subject->runSeed(
            $this->config,
            'SeedOne',
            true,
        );

        $this->assertTestLog([]);
    }

    /**
     * @test
     * @testdox runSeed() rolls back all changes on exception in seed
     */
    public function runSeedException(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Yramid\Test\Seeds\FailingSeed::run');

        $this->subject->runSeed(
            $this->config,
            'FailingSeed',
            false,
        );
    }

    /**
     * @test
     * @testdox runSeed() fails on non-existing seed
     */
    public function runSeedNonExistingSeed(): void
    {
        $this->expectExceptionObject(
            new RuntimeException('Unknown Seed ' . __FUNCTION__),
        );
        $this->subject->runSeed(
            $this->config,
            __FUNCTION__,
            true,
        );
    }

    /**
     * @test
     * @testdox createSeed() creates valid file
     */
    public function createSeed(): void
    {
        $seedData = $this->subject->createSeed(
            $this->config,
            'NewSeed',
        );

        $this->assertSame(
            'NewSeed',
            $seedData->name,
        );

        $this->subject->runSeed(
            $this->config,
            'NewSeed',
            false,
        );
    }

    /**
     * @test
     * @testdox createSeed() fails on invalid name
     */
    public function createSeedInvalidName(): void
    {
        $this->expectExceptionObject(new InvalidName('new_seed'));
        $this->subject->createSeed(
            $this->config,
            'new_seed',
        );
    }

    /**
     * @test
     * @testdox createSeed() fails when trying to create an existing seed
     */
    public function createDuplicateSeed(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('There is already a seed named SeedOne');

        $this->subject->createSeed(
            $this->config,
            'SeedOne',
        );
    }
}
