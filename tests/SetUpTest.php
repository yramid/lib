<?php

declare(strict_types=1);

namespace Yramid\Test;

use org\bovigo\vfs\vfsStream;
use Yramid\ConfigAccessor;

/**
 * @testdox Yramid
 */
class SetUpTest extends TestCase
{
    protected function getConfig(): array
    {
        vfsStream::setup('root');
        return include 'test_config.php';
    }

    /**
     * @test
     * @testdox setUp() sets up everything
     */
    public function setUpOnce(): void
    {
        $this->assertDirectoryDoesNotExist(
            ConfigAccessor::getSeedPath($this->config),
        );

        $this->assertDirectoryDoesNotExist(
            ConfigAccessor::getMigrationPath($this->config),
        );

        $this->subject->setUp($this->config);

        $this->assertDirectoryExists(
            ConfigAccessor::getSeedPath($this->config),
        );

        $this->assertDirectoryExists(
            ConfigAccessor::getMigrationPath($this->config),
        );
    }

    /**
     * @test
     * @testdox setUp() called twice behaves well
     */
    public function setUpTwice(): void
    {
        $this->subject->setUp($this->config);

        touch(
            ConfigAccessor::getMigrationPath($this->config) . '/Ping',
        );

        touch(
            ConfigAccessor::getSeedPath($this->config) . '/Ping',
        );

        $this->subject->setUp($this->config);

        $this->assertFileExists(
            ConfigAccessor::getSeedPath($this->config) . '/Ping',
        );

        $this->assertFileExists(
            ConfigAccessor::getMigrationPath($this->config) . '/Ping',
        );
    }
}
