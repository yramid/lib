<?php

declare(strict_types=1);

namespace Yramid\Test;

use Closure;
use PHPUnit\Framework\TestCase as PhpUnitTestCase;
use Yramid\ConfigAccessor;
use Yramid\Migration\MigrationData;
use Yramid\Yramid;

abstract class TestCase extends PhpUnitTestCase
{
    protected Yramid $subject;

    protected array $config;

    abstract protected function getConfig(): array;

    protected function setUp(): void
    {
        $pdo = (new \ReflectionClass(ConfigAccessor::class))
            ->getProperty('pdo');
        $pdo->setAccessible(true);
        $pdo->setValue(null);

        parent::setUp();

        $this->subject = new Yramid();
        $this->config = $this->getConfig();
    }

    protected function assertStatus(array $expected): void
    {
        $this->assertSame(
            $expected,
            array_column(
                $this->subject->getMigrationStatus($this->config),
                'status',
                'serial',
            ),
        );
    }

    protected function assertTestLog(array $expected): void
    {
        $this->assertSame(
            $expected,
            ConfigAccessor::getPdo($this->config)
                ->query('SELECT * FROM test_log')
                ->fetchAll(\PDO::FETCH_COLUMN),
        );
    }

    protected function postMigrateCallback(?array &$log = null): Closure
    {
        $log ??= [];

        return static function (
            MigrationData $migrationData,
            string $oldStatus,
            string $newStatus,
            float $runTime,
        ) use (&$log): void {
            $log[$migrationData->serial] = [
                $oldStatus,
                $newStatus
            ];
        };
    }
}
