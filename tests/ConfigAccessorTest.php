<?php

declare(strict_types=1);

namespace Yramid\Test;

use PDO;
use PHPUnit\Framework\TestCase;
use Yramid\ConfigAccessor;
use Yramid\Exception\InvalidConfigValue;

class ConfigAccessorTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $property = (new \ReflectionClass(ConfigAccessor::class))
            ->getProperty('pdo');
        $property->setAccessible(true);
        $property->setValue(null);
    }

    /**
     * @test
     * @testdox getPdo returns a PDO singleton instance for a DSN
     */
    public function getPdoWithDsn(): void
    {
        $config = ['connection' => 'sqlite::memory:'];

        $pdo = ConfigAccessor::getPdo($config);

        $this->assertSame(
            $pdo,
            ConfigAccessor::getPdo($config),
            'Returns a PDO singleton'
        );
    }

    /**
     * @test
     * @testdox getPdo returns a PDO singleton instance for a existing connection
     */
    public function getPdoWithInstance(): void
    {
        $configPdo = new PDO('sqlite::memory:');

        $config = ['connection' => $configPdo];

        $pdo = ConfigAccessor::getPdo($config);

        $this->assertSame(
            $configPdo,
            $pdo,
            'Returns a PDO singleton'
        );
    }

    /**
     * @test
     * @testdox getPdo returns a PDO singleton instance for a existing connection
     */
    public function getPdoFromFactory(): void
    {
        $configPdo = new PDO('sqlite::memory:');

        $config = ['connection' => static fn(): PDO => $configPdo];

        $pdo = ConfigAccessor::getPdo($config);

        $this->assertSame(
            $configPdo,
            $pdo,
            'Returns a PDO singleton'
        );
    }

    /**
     * @test
     * @testdox getPdo throws an exception on missing config key
     */
    public function getPdoMissingValue(): void
    {
        $this->expectExceptionObject(
            new InvalidConfigValue('Missing or invalid connection'),
        );

        ConfigAccessor::getPdo([]);
    }

    /**
     * @test
     * @testdox getPdo throws an exception on invalid config value
     */
    public function getPdoInvalidValue(): void
    {
        $this->expectExceptionObject(
            new InvalidConfigValue('Missing or invalid connection'),
        );

        ConfigAccessor::getPdo(['connection' => 123]);
    }

    /**
     * @test
     * @testdox getMigrationPath returns the config value
     */
    public function getMigrationPath(): void
    {
        $path = __DIR__;

        $this->assertSame(
            $path,
            ConfigAccessor::getMigrationPath(
                ['migrations' => ['path' => $path]],
            ),
        );
    }

    /**
     * @test
     * @testdox getMigrationPath fails on missing config value
     */
    public function getMigrationPathFails(): void
    {
        $this->expectExceptionObject(new InvalidConfigValue('Invalid or missing migrations path'));
        ConfigAccessor::getMigrationPath(
            ['migrations' => []],
        );
    }

    /**
     * @test
     * @testdox getSeedPath returns the config value
     */
    public function getSeedPath(): void
    {
        $path = __DIR__;

        $this->assertSame(
            $path,
            ConfigAccessor::getSeedPath(
                ['seeds' => ['path' => $path]],
            ),
        );
    }

    /**
     * @test
     * @testdox getSeedPath fails on missing config value
     */
    public function getSeedPathFails(): void
    {
        $this->expectExceptionObject(new InvalidConfigValue('Invalid or missing seeds path'));
        ConfigAccessor::getSeedPath(
            ['seeds' => []],
        );
    }

    /**
     * @test
     * @testdox getMigrationTemplate returns the config and default value
     */
    public function getMigrationTemplate(): void
    {
        $this->assertSame(
            __FILE__,
            ConfigAccessor::getMigrationTemplate(
                ['migrations' => ['template' => __FILE__]],
            ),
        );

        $this->assertSame(
            realpath(__DIR__ . '/../src/Migration/migration.tpl'),
            ConfigAccessor::getMigrationTemplate(
                ['migrations' => []],
            ),
        );
    }

    /**
     * @test
     * @testdox getMigrationTemplate fails when not a file
     */
    public function getMigrationTemplateFailsOnNotAFile(): void
    {
        $this->expectExceptionObject(
            new InvalidConfigValue(
                'Invalid migrations template file: ' . __DIR__,
            ),
        );

        ConfigAccessor::getMigrationTemplate(
            ['migrations' => ['template' => __DIR__]],
        );
    }

    /**
     * @test
     * @testdox getSeedTemplate returns the config and default value
     */
    public function getSeedTemplate(): void
    {
        $this->assertSame(
            __FILE__,
            ConfigAccessor::getSeedTemplate(
                ['seeds' => ['template' => __FILE__]],
            ),
        );

        $this->assertSame(
            realpath(__DIR__ . '/../src/Seed/seed.tpl'),
            ConfigAccessor::getSeedTemplate(
                ['seeds' => []],
            ),
        );
    }

    /**
     * @test
     * @testdox getMigrationTemplate fails when not a file
     */
    public function getSeedTemplateFailsOnNotAFile(): void
    {
        $this->expectExceptionObject(
            new InvalidConfigValue(
                'Invalid seeds template file: ' . __DIR__,
            ),
        );

        ConfigAccessor::getSeedTemplate(
            ['seeds' => ['template' => __DIR__]],
        );
    }

    /**
     * @test
     * @testdox getMigrationNamespace returns the config value
     */
    public function getMigrationNamespace(): void
    {
        $this->assertSame(
            __NAMESPACE__,
            ConfigAccessor::getMigrationNamespace(
                ['migrations' => ['namespace' => __NAMESPACE__]],
            ),
        );
    }

    /**
     * @test
     * @testdox getMigrationNamespace fails on missing config value
     */
    public function getMigrationNamespaceMissingConfig(): void
    {
        $this->expectExceptionObject(new InvalidConfigValue('Missing migrations namespace'));
        ConfigAccessor::getMigrationNamespace(
            ['migrations' => []],
        );
    }

    /**
     * @test
     * @testdox getMigrationNamespace fails on invalid value
     */
    public function getMigrationNamespaceInvalidValue(): void
    {
        $this->expectExceptionObject(new InvalidConfigValue('Invalid migrations namespace: 1+1=3'));
        ConfigAccessor::getMigrationNamespace(
            ['migrations' => ['namespace' => '1+1=3']],
        );
    }

    /**
     * @test
     * @testdox getSeedNamespace returns the config value
     */
    public function getSeedNamespace(): void
    {
        $this->assertSame(
            __NAMESPACE__,
            ConfigAccessor::getSeedNamespace(
                ['seeds' => ['namespace' => __NAMESPACE__]],
            ),
        );
    }

    /**
     * @test
     * @testdox getSeedNamespace fails on missing config value
     */
    public function getSeedNamespaceMissingConfig(): void
    {
        $this->expectExceptionObject(new InvalidConfigValue('Missing seeds namespace'));
        ConfigAccessor::getSeedNamespace(
            ['seeds' => []],
        );
    }

    /**
     * @test
     * @testdox getSeedNamespace fails on invalid value
     */
    public function getSeedNamespaceInvalidValue(): void
    {
        $this->expectExceptionObject(new InvalidConfigValue('Invalid seeds namespace: 1+1=3'));
        ConfigAccessor::getSeedNamespace(
            ['seeds' => ['namespace' => '1+1=3']],
        );
    }

    /**
     * @test
     * @testdox relaxAboutMissingMigrations returns the config value and default
     */
    public function relaxAboutMissingMigrations(): void
    {
        $this->assertFalse(
            ConfigAccessor::relaxAboutMissingMigrations(
                ['migrations' => []],
            ),
        );

        $this->assertTrue(
            ConfigAccessor::relaxAboutMissingMigrations(
                ['migrations' => ['relax' => true]],
            ),
        );

        $this->assertFalse(
            ConfigAccessor::relaxAboutMissingMigrations(
                ['migrations' => ['relax' => false]],
            ),
        );
    }
}
