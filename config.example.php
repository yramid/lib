<?php

return [
    /**
     * The database connection to use.
     * Either a DSN, a \PDO instance or a callable that returns a \PDO instance.
     *
     * Mandatory
     */
    'connection' => 'sqlite::memory:',

    'migrations' => [
        /**
         * The path where migration files are located
         *
         * Mandatory
         */
        'path' => __DIR__ . '/db/migrations',

        /**
         * The namespace for migration classes without leading or trailing backslashes
         *
         * Mandatory
         */
        'namespace' => 'My\Package\Migration',

        /**
         * Which template file to use when creating new migrations.
         * The default template is used when omitted or `null`
         *
         * Optional
         */
        'template' => __DIR__ . '/db/migration.tpl',

        /**
         * When set to false migrations and rollbacks will fail on
         * missing migration files/classes instead of ignoring them.
         *
         * Optional, defaults to false
         */
        'relax' => false,
    ],
    'seeds' => [
        /**
         * The path where seed files are located
         *
         * Mandatory
         */
        'path' => __DIR__ . '/db/seeds',

        /**
         * The namespace for seed classes without leading or trailing backslashes
         *
         * Mandatory
         */
        'namespace' => 'My\Package\Seed',

        /**
         * Which template file to use when creating new migrations.
         * The default template is used when omitted or `null`
         *
         * Optional
         */
        'template' => __DIR__ . '/db/seed.tpl',
    ],
];
